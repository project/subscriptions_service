<?php
/**
 * This function is the same function subscriptions uses to submit an event
 * to the queue.
 *
 * @see subscriptions_queue()
 */
function subscriptions_service_queue($event) {
  global $user;

  if (isset($event['node']->nid) && strpos('  '. variable_get('subscriptions_blocked_nodes', '') .' ', ' '. $event['node']->nid .' ')) {
    return;
  }

  $event += array(
    'uid' => $user->uid,
    'load_args' => '',
  );

  foreach (module_implements('subscriptions_queue_alter') as $module) {
    $function = $module .'_subscriptions_queue_alter';
    $function($event);
    if (empty($event)) {
      return;  // $event was cleared, forget it
    }
  }

  if (is_array($event['load_args'])) {
    $event['load_args'] = serialize($event['load_args']);
  }
  if (!empty($event['noqueue_uids'])) {
    // Allow hook_subscriptions_queue_alter() modules to set uids that won't get any notifications queued:
    $noqueue_uids_where = "s.recipient_uid NOT IN (". implode(', ', array_fill(0, count($event['noqueue_uids']), '%d')) .")";
  }

  foreach (module_implements('subscriptions') as $subs_module) {
    $subs_module_query = module_invoke($subs_module, 'subscriptions', 'queue', $event);
    if (!isset($subs_module_query)) {
      continue;
    }
    foreach ($subs_module_query as $module => $module_query) {
      foreach ($module_query as $field => $query) {
        $join = empty($query['join']) ? '' : $query['join'];
        $where = empty($query['where']) ? array() : array($query['where']);
        $args = array($event['load_function'], $event['load_args'], $event['is_new'], $module, $field);
        // author-specific subscriptions trigger on comments, when the node author is subscribed to:
        $args[] = ($module == 'node' && $event['type'] == 'comment' && isset($event['node']->uid) ? $event['node']->uid : $event['uid']);
        if (!empty($query['value'])) {
          $where[] = "s.value = '%s'";
          $args[] = $query['value'];
        }
        if (!empty($query['args'])) {
          $args = array_merge($args, $query['args']);
        }
        if ($user->uid && !_subscriptions_get_setting('send_self', $user)) {
          $where[] = "s.recipient_uid != %d";
          $args[] = $user->uid;
        }
        if (!empty($event['noqueue_uids'])) {
          $where[] = $noqueue_uids_where;
          $args = array_merge($args, $event['noqueue_uids']);
        }
        $conditions = implode(' AND ', $where);
        $sql = "
          INSERT INTO {subscriptions_queue} (uid, name, mail, language, module, field, value, author_uid, send_interval, digest, last_sent, load_function, load_args, is_new)
          SELECT u.uid, u.name, u.mail, u.language, s.module, s.field, s.value, s.author_uid, s.send_interval, su.digest, COALESCE(sls.last_sent, 0), '%s', '%s', '%d'
          FROM {subscriptions} s
          INNER JOIN {subscriptions_user} su ON s.recipient_uid = su.uid
          INNER JOIN {users} u ON su.uid = u.uid
          $join
          LEFT JOIN {subscriptions_last_sent} sls ON su.uid = sls.uid AND s.send_interval = sls.send_interval
          WHERE
            s.module = '%s' AND
            s.field = '%s' AND
            s.author_uid IN (%d, -1) AND $conditions";
        $result = db_query($sql, $args);
        $affected_rows = db_affected_rows();
        /*  for debugging:
        $sql = db_prefix_tables($sql);
        _db_query_callback($args, TRUE);
        $sql = preg_replace_callback(DB_QUERY_REGEXP, '_db_query_callback', $sql);
        drupal_set_message("$sql<br />". $affected_rows .' row(s) inserted.');
        /**/
      }
    }
  }
}